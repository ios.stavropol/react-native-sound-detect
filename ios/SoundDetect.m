// SoundDetect.m

#import "SoundDetect.h"
#import <AVFoundation/AVFoundation.h>

@implementation SoundDetect

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(isBackgroundSoundExist:(RCTResponseSenderBlock)callback)
{
    // TODO: Implement some actually useful functionality
    BOOL isOtherAudioPlaying = [[AVAudioSession sharedInstance] isOtherAudioPlaying];
    callback(@[[NSNull null], [NSNumber numberWithBool:isOtherAudioPlaying]]);
}

@end
