// SoundDetectModule.java

package com.reactlibrary;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import android.content.Context;
import android.media.AudioManager;

public class SoundDetectModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;

    public SoundDetectModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "SoundDetect";
    }

    @ReactMethod
    public void isBackgroundSoundExist(Callback callback) {
        // TODO: Implement some actually useful functionality
        AudioManager am = (AudioManager) this.reactContext.getSystemService(Context.AUDIO_SERVICE);

        callback.invoke(am.isMusicActive());
    }
}
